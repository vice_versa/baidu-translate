# -*- coding: utf-8 -*-

from settings import TRANSLATE_URI, TRANSLATE_FROM, TRANSLATE_TO
import requests
import grequests


class TranslatorException(Exception):
    pass


class YandexTranslator(object):

    def __init__(self):
        pass

    def get_translate_result(self, **kwargs):
        response = requests.get(TRANSLATE_URI, params=kwargs)
        self._check_response(response)
        output = response.json
        trans_result = output['text']
        return trans_result

    def translate(self, text, from_lang=TRANSLATE_FROM, to_lang=TRANSLATE_TO):

        lang = "%s-%s" % (from_lang, to_lang)
        kwargs = {
                  'text': text,
                  "lang": lang,
                  }
        trans_result = self.get_translate_result(**kwargs)
        return " ".join(trans_result)

    def translateArray(self, text_list, from_lang=TRANSLATE_FROM, to_lang=TRANSLATE_TO):
        lang = "%s-%s" % (from_lang, to_lang)
        kwargs = {"lang": lang}
        async_list = []
        for text in text_list:
            kwargs.update(text=text)
            action_item = grequests.get(TRANSLATE_URI, params=kwargs)
            async_list.append(action_item)
        response_list = grequests.map(async_list)
        return [response.json["text"][0] for response in response_list]

    def _check_response(self, response):
        output = response.json
        if not response.ok:
            if output is None:
                error = response.reason
            else:
                error = '{0}\n{1}'.format(output['error'],
                                          output['error_description']
                                          )
            raise TranslatorException(error)

        elif "error_code" in output:
            error = '{0}\n{1}'.format(output['error_code'],
                                      output['error_msg']
                                      )
            raise TranslatorException(error)


if __name__ == "__main__":

    translator = YandexTranslator()
    result = translator.translate(u"Hello World", "en", "ru")
    print result

    result = translator.translateArray([u"Hello", u"World"], "en", "ru")
    print result
