# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from django.conf import settings
import requests
from xml.etree import ElementTree as ET


class TranslatorException(Exception):
    pass


class BingTranslator(object):
    client_id = settings.BING_CLIENT_ID
    client_secret = settings.BING_SECRET_TOKEN
    SCOPE = 'http://api.microsofttranslator.com'
    GRANT_TYPE = 'client_credentials'
    DATAMARKET_ACCESS_URI = 'https://datamarket.accesscontrol.windows.net/v2/OAuth2-13'
    TRANSLATE_URI = 'http://api.microsofttranslator.com/V2/Http.svc/Translate'
    TRANSLATE_ARRAY_URI = 'http://api.microsofttranslator.com/V2/Http.svc/TranslateArray'

    def __init__(self):        
        # store taken access_token
        self.access_token = None
        self.expires_at = datetime.now()

    def _is_valid_access_token(self):
        """
        Is stored access_token still valid or not
        """
        # we should compare current time and expires_at minus 1 minute
        # if (current time < expires_at - 1 min.) then we have one minute to use old token
        return datetime.now() < (self.expires_at - timedelta(minutes=1))

    def _check_response(self, response):
        if not response.ok:
            output = response.json
            if output is None:
                error = response.reason
            else:
                error = '{0}\n{1}'.format(output['error'], output['error_description'])
            raise TranslatorException(error)

    def _retrieve_access_token(self):
        # if stored access_token not expired then use it
        if self._is_valid_access_token():
            return self.access_token
        input = {
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'scope': self.SCOPE,
            'grant_type': self.GRANT_TYPE
        }
        response = requests.post(self.DATAMARKET_ACCESS_URI, data=input)
        self._check_response(response)
        output = response.json
        self.expires_at = datetime.now() + timedelta(seconds=int(output['expires_in']))
        self.access_token = output['access_token']

    def translate(self, text, from_lang='zh-CHT', to_lang='ru'):
        self._retrieve_access_token()
        headers = {'Authorization': 'Bearer {0}'.format(self.access_token)}
        parameters = {
            'text': text,
            'form': from_lang,
            'to': to_lang
        }
        response = requests.get(self.TRANSLATE_URI, headers=headers, params=parameters)
        self._check_response(response)
        translated_text = ET.fromstring(response.text.encode('utf-8')).text
        return translated_text

    def translateArray(self, texts, from_lang='zh-CHT', to_lang='ru'):
        self._retrieve_access_token()
        headers = {
            'Authorization': 'Bearer {0}'.format(self.access_token),
            'content-type': 'text/xml'
        }
        formated_texts = ['<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/Arrays">{0}</string>'.format(text) for text in texts]
        request_body = '''
            <TranslateArrayRequest>
                <AppId />
                <From>{0}</From>
                <Options>
                    <Category xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2" />
                    <ContentType xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2">text/plain</ContentType>
                    <ReservedFlags xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2" />
                    <State xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2" />
                    <Uri xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2" />
                    <User xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2" />
                </Options>
                <Texts>
                    {1}
                </Texts>
                <To>{2}</To>
            </TranslateArrayRequest>
        '''.format(from_lang, ''.join(formated_texts), to_lang)
        response = requests.post(self.TRANSLATE_ARRAY_URI, headers=headers, data=request_body)
        self._check_response(response)
        array_of_translate_array_response = ET.fromstring(response.text.encode('utf-8'))
        xmlns = '{http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2}'
        translated_texts = array_of_translate_array_response.findall('./{0}TranslateArrayResponse/{0}TranslatedText'.format(xmlns))
        return [translated_text.text for translated_text in translated_texts]
