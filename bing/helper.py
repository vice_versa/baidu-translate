# -*- coding: utf-8 -*-
import urllib, httplib
import json
import re
from translator.settings import BING_CLIENT_ID, BING_SECRET_TOKEN
from xml.dom.minidom import *

class AccessTokenHelper:

    @staticmethod
    def get_token():
        params = {
            'client_id': BING_CLIENT_ID,
            'client_secret': BING_SECRET_TOKEN,
            'scope': 'http://api.microsofttranslator.com',
            'grant_type':'client_credentials'
        }
        params = urllib.urlencode(params)
        connection = httplib.HTTPSConnection('datamarket.accesscontrol.windows.net')
        connection.request('POST', '/v2/OAuth2-13', params)
        result = json.loads(connection.getresponse().read())
        return result['access_token']


class BingTranslator(object):

    def translate(self, text, from_lang, to):
        token = AccessTokenHelper.get_token()
        headers = {'Authorization':'Bearer %s' % token}
        connection = httplib.HTTPConnection('api.microsofttranslator.com')
        params = {
            'from': from_lang,
            'to': to,
            'text': text
        }
        url = '/V2/Http.svc/Translate?' + urllib.urlencode(params)
        connection.request('GET', url, headers=headers)
        xml = connection.getresponse().read()
        return re.sub(r"<.+?>", " ", xml).strip()


    def translate_array(self, texts, from_lang, to):
        token = AccessTokenHelper.get_token()
        headers = {'Authorization':'Bearer %s' % token, 'Content-type': 'text/xml'}
        connection = httplib.HTTPConnection('api.microsofttranslator.com')
        url = '/V2/Http.svc/TranslateArray'
        connection.request('POST', url, self.build_request_xml(from_lang, to, texts), headers)
        response = connection.getresponse().read()

        return self.get_array_from_xml(response)


    def get_array_from_xml(self, string_xml):
        xml = parseString(string_xml)
        nodes = xml.getElementsByTagName('TranslatedText')
        response_array = []
        for node in nodes:
            response_array.append(self.get_text_from_nodes(node.childNodes))
        return response_array


    def get_text_from_nodes(self, nodelist):
        rc = []
        for node in nodelist:
            if node.nodeType == node.TEXT_NODE:
                rc.append(node.data)
        return ''.join(rc)


    def build_request_xml(self, from_lang, to, texts):
        header = """<?xml version="1.0" encoding="utf-8"?>
<TranslateArrayRequest>
        <AppId />
        <From>%s</From>
        <Options>
                <Category xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2" />
                <ContentType xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2">text/html</ContentType>
                <ReservedFlags xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2" />
                <State xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2" />
                <Uri xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2" />
                <User xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2" />
        </Options>
        <Texts>""" % from_lang

        temp_string = """<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/Arrays">%s</string>"""

        footer = """
        </Texts>
        <To>%s</To>
</TranslateArrayRequest>""" % to

        content = ""

        for item in texts:
            content += temp_string % item.replace("<", "&lt;").replace(">", "&gt;")

        return header + content + footer